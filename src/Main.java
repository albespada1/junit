/**
 * 
 * @author Alberto
 * @version 1.0
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Este metodo suma dos parametros pasados por connsolas
	 * 
	 * @param num1
	 * @param num2
	 * @return Retorna los dos numero sumados
	 */
	public int suma(int num1, int num2) {
		return num1 + num2;
	}
	
	/**
	 * Este metoodo resta dos parametros paados por consola
	 * 
	 * @param num1
	 * @param num2
	 * @return Retorna los dos numero restados
	 */
	public int restar(int num1, int num2) {
		return num1 - num2;
	}
	
	/**
	 * Este metodo multiplicar dos parametros pasados por consola
	 * 
	 * @param num1
	 * @param num2
	 * @return Retorna los dos numero multiplicados
	 */
	public int multiplicar(int num1, int num2) {
		return num1 * num2;
	}
	
	/**
	 * Este metodoo divide dos parametros pasados por consola
	 * 
	 * @param num1
	 * @param num2
	 * @return Retorna los dos numeros divididos
	 */
	public int dividir(int num1, int num2) {
		return num1 / num2;
	}

}
