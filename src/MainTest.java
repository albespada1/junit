import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MainTest {

	@Test
	void testSuma() {
		int num1 = 1;
		int num2 = 1;
		Main test = new Main();
		assertEquals(test.suma(num1, num2), 2);
		//fail("Not yet implemented");
	}

	@Test
	void testRestar() {
		int num1 = 1;
		int num2 = 1;
		Main test = new Main();
		assertEquals(test.restar(num1, num2), 0);
		//fail("Not yet implemented");
	}

	@Test
	void testMultiplicar() {
		int num1 = 1;
		int num2 = 1;
		Main test = new Main();
		assertEquals(test.multiplicar(num1, num2), 1);
		//fail("Not yet implemented");
	}

	@Test
	void testDividir() {
		int num1 = 1;
		int num2 = 1;
		Main test = new Main();
		assertEquals(test.dividir(num1, num2), 1);
		//fail("Not yet implemented");
	}

}
